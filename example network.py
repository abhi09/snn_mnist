#example network assuming a picture like
#   0001
#   0010
#   0100
#   1000
#and refractory period of 2
from random import random

first_layer = [iz_onepass(0.001) for i in range(4)]
first_layer[0].spike_train = [0,0,0,0,0,0,1,0]
first_layer[1].spike_train = [0,0,0,0,1,0,0,0]
first_layer[2].spike_train = [0,0,1,0,0,0,0,0]
first_layer[3].spike_train = [1,0,0,0,0,0,0,0]

second_layer = [iz_onepass(0.001) for i in range(2)]
second_layer[0].add_parent(first_layer[0],random())
second_layer[0].add_parent(first_layer[1],random())
second_layer[1].add_parent(first_layer[2],random())
second_layer[1].add_parent(first_layer[3],random())

third_layer = [iz_onepass(0.001) for i in range(2)]
third_layer[0].add_parent(second_layer[0],random())
third_layer[0].add_parent(second_layer[1],random())
third_layer[1].add_parent(second_layer[0],random())
third_layer[1].add_parent(second_layer[1],random())

for neuron in second_layer:
    neuron.get_spikes()

for neuron in third_layer:
    neuron.get_spikes()
