
class iz_realtime:
    def init(self,dt): #dt is the time step

        #hyperparameters

        self.dt = dt #ms
        self.a = 0.02 #recovery speed
        self.b = 0.2 #u,v correlation
        self.d = 2 #post spike reset
        self.rest_voltage = -65 #mV, c in paper
        self.firing_voltage = 30 #mV

        #neuron state

        self.v = self.rest_voltage
        self.u = self.b * self.v #u=b*v

        #parents
        self.parents = []
        self.weights = []

        self.latest_timestep_spiked = False

    def step(self):
        total_input_current = sum([self.parents[i].latest_timestep_spiked * self.weights[i] for i in range(len(self.parents))])

        dt,u,v = self.dt,self.u,self.v

        self.v += dt * (0.04*v*v + 5*v + 140 - u + total_input_current)
        self.u += dt * a * (self.b*v - u )

        if self.v > self.firing_voltage:
            self.v = self.rest_voltage
            self.u += self.d
            self.latest_timestep_spiked = True
        else:
            self.latest_timestep_spiked = False

    def add_parent(self,p,w):
        self.parents.append(p)
        self.weights.append(w)
