import STDP as stdp
import neuron as iz
import random as random
import keras
import pickle


class Main:
    def __init__(self,Altp, Altd, beta, sigma, r, tou_plus,row):
        self.Altp = Altp
        self.Altd = Altd
        self.beta = beta
        self.sigma = sigma
        self.r = r
        self.tou_plus = tou_plus
        self.stdp = stdp.STDP(Altp, Altd, beta, sigma, r, tou_plus)
        self.num_labels = 10

        self.first_layer_neuron = [iz.iz_onepass(0.001) for i in range(row)]
        self.second_layer_neuron = [iz.iz_onepass(0.001) for i in range(row // 2)]

        for i in range(row//2):
            self.second_layer_neuron[i].add_parent(self.first_layer_neuron[2 * i], random.random())
            self.second_layer_neuron[i].add_parent(self.first_layer_neuron[2 * i + 1], random.random())

        self.third_layer_neuron = [iz.iz_onepass(0.001) for i in range(self.num_labels)]
        for i in range(self.num_labels):
            for j in self.second_layer_neuron:
                self.third_layer_neuron[i].add_parent(j, random.random())

        self.load_data()

    def load_data(self):
        (x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data('MNIST-data-%d')
        self.xtrain = x_train
        self.ytrain = y_train

        self.xtest = x_test
        self.ytest = y_test
        return

    def layer_1(self,row, r):
        curr_r = 0
        new_row = []
        for i in range((len(row) * r)):
            if curr_r > 0:
                curr_r -= 1
                new_row.append(0)
            elif row[i // r] > 0:
                curr_r = r
                new_row.append(1)
            else:
                new_row.append(0)
        return new_row

    def do_one_pass(self, image, label):
        (n, m) = image.shape
        # Refractory Period
        # Layer 1

        for i,row in enumerate(image):
            spike_train = self.layer_1(row, self.r)
            self.first_layer_neuron[i].spike_train = spike_train
        # Layer 2

        for i in range(n//2):
            self.second_layer_neuron[i].get_spikes()
        # print(middle_spike_trains)
        # Layer 3
        for i in range(self.num_labels):
            if i == label:
                self.third_layer_neuron[i].get_spikes()
            else:
                self.third_layer_neuron[i].spike_train = [0] * m * self.r

        for third_neuron in self.third_layer_neuron:
            l = len(third_neuron.parents)
            for j in range(l):
                third_neuron.weights[j] = self.stdp.run_stdp(third_neuron.spike_train,
                                                             third_neuron.parents[j].spike_train, third_neuron.weights[j])

        for second_neuron in self.second_layer_neuron:
            l = len(second_neuron.parents)
            for j in range(l):
                second_neuron.weights[j] = self.stdp.run_stdp(second_neuron.spike_train,
                                                              second_neuron.parents[j].spike_train, second_neuron.weights[j])

    def run_model(self):
        for idx in range(len(self.xtrain)):
            self.do_one_pass(self.xtrain[idx], self.ytrain[idx])
        #Store Model
        pickle.dump(self.first_layer_neuron,open('first_layer.obj', 'w'))
        pickle.dump(self.second_layer_neuron,open('second_layer.obj', 'w'))
        pickle.dump(self.third_layer_neuron, open('third_layer.obj', 'w'))

ibj = Main(1.1, 1.2, 1.5, 2, 2, 1.5, 28)
ibj.run_model()
