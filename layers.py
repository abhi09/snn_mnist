def layer1(row, r):
    curr_r = 0
    new_row = []
    for i in range((len(row) * r)):
        if curr_r > 0:
            curr_r -= 1
            new_row.append(0)
        elif row[i // r] > 0:
            curr_r = r
            new_row.append(1)
        else:
            new_row.append(0)
    return new_row

def layer_2(spike_trains, n, m, k):
    middle_spikes = []
    for i in range(n // k):
        pre_spikes = []
        for j in range(k):
            pre_spikes.append(spike_trains[i * k + j])
        # Merge K Spike Trains
        middle_spikes.append(izhikevich(pre_spikes))
    return middle_spikes

