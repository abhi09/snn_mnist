import numpy as np

class STDP:
    def __init__(self, Altp, Altd, beta, sigma, r, tou_plus):
        self.Altp = Altp
        self.Altd = Altd
        self.beta = beta
        self.sigma = sigma
        self.r = r
        self.tou_plus = tou_plus

    #Have quesries about t_i_f
    def calculate_tif(self,spike_train_i,t_i_f):
        for i in range(t_i_f+1,len(spike_train_i)):
            if spike_train_i[i] == 1:
                return i
        return -1
    def run_stdp(self, spike_train_i, spike_train_j, Wij):
        for t_i,p_i in enumerate(spike_train_i):
            for t_j,p_j in enumerate(spike_train_j):
                if p_i == 1 and p_j == 1:
                    delta_time = (t_j - t_i) * self.r
                    if delta_time > 0:
                        del_weight = self.Altp * np.exp(
                            -(delta_time) * 1.0 / self.tou_plus) / Wij
                    else:
                        del_weight = -self.Altd * np.exp(-self.beta)
                    Wij += del_weight
        return Wij


#TEST
'''
s = STDP(2,1.5,1.2,3,2,2)
spike_train_i = [1,0,1,0]
spike_train_j = [0,1,0,1]
Wij = .1
print(s.run_stdp(spike_train_i,spike_train_j,Wij))
'''