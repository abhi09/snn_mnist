#This file implements the iz neuron model
#https://courses.cs.washington.edu/courses/cse528/07sp/izhi1.pdf

#in this class, you have to manually set the initial spikes
class iz_onepass:
    def __init__(self,dt): #dt is the time step

        #hyperparameters

        self.dt = dt #ms
        self.a = 0.02 #recovery speed
        self.b = 0.2 #u,v correlation
        self.d = 2 #post spike reset
        self.rest_voltage = -65 #mV, c in paper
        self.firing_voltage = 30 #mV

        #neuron state

        self.v = self.rest_voltage
        self.u = self.b * self.v #u=b*v

        #parents
        self.parents = []
        self.weights = []

        self.spike_train = []

    def integrate(self,I): #I is the injected current

        #solve ode using simplest 1 step euler method
        #then return boolean variable indication this neuron fired or not

        dt,u,v = self.dt,self.u,self.v

        self.v += dt * (0.04*v*v + 5*v + 140 - u + I)
        self.u += dt * self.a * (self.b*v - u )

        if self.v > self.firing_voltage:
            self.v = self.rest_voltage
            self.u += self.d
            return True

        return False

    def add_parent(self,p,w):
        self.parents.append(p)
        self.weights.append(w)

    #input is a list of list of spikes
    #weights is a list
    #use the spike trains of parents to get own spike train
    def get_spikes(self):
        if len(self.parents)==0:
            return self.spike_train

        self.spike_train = []
        spike_train_length = len(self.parents[0].spike_train)

        for t in range(spike_train_length):
            I = 0
            for i in range(len(self.parents)):
                I += self.parents[i].spike_train[t] * self.weights[i]

            self.spike_train.append(self.integrate(I))
            print("at time "+str(t)+" v="+str(self.v)+" u="+str(self.u))

        #return self.spike_train
